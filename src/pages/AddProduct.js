import { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {

    const navigate = useNavigate();
    const { user } = useContext(UserContext);
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    // const [imageUrl, setImageUrl] = useState("");

    function createProduct(e) {
        const token = localStorage.getItem('token');
        if (!token) {
            console.error("Token not found");
            return;
        }

        e.preventDefault()
        fetch('https://backend-qs14.onrender.com/product/', {

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
                // imageUrl: imageUrl
            })

        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        icon: "success",
                        title: "Product Added"
                    })
                    navigate("/product");
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Unsuccessful Product Creation",
                        text: data.message

                    }).catch(error => {
                        console.error('Error:', error);
                    });
                }
            })
        setName("")
        setDescription("")
        setPrice(0);
    }
    // const handleFileInputChange = (e) => {
    //     const file = e.target.files[0]
    //     setImageUrl(file.name)
    // }
    // console.log(imageUrl)
    return (
        (user.isAdmin === true)
            ?
            <>
                <div className="add-product-container">
                    <h1 className="my-5 text-center">Add Product</h1>
                    <Form onSubmit={e => createProduct(e)}>
                        <Form.Group>
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={e => { setName(e.target.value) }} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description:</Form.Label>
                            <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => { setDescription(e.target.value) }} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => { setPrice(e.target.value) }} />
                            {/* <input type='file' id='productImage' name='productImage' accept='image/*' onChange={handleFileInputChange} /> */}
                        </Form.Group>

                        <Button variant="primary" type="submit" className="my-1">Submit</Button>
                    </Form>
                </div>
            </>
            :
            <Navigate to="/product" />
    )
}
